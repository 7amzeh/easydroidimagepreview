package com.hamzeh.easydroid.preview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hamzeh.easydriod.preview.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class PreviewFragment extends Fragment {
    public static final String TAG = PreviewFragment.class.getSimpleName();

    public static final String EXTRA_RECT = "RECT";
    public static final String EXTRA_URL = "URL";
    public static final String EXTRA_FOCUSED = "FOCUSED";

    private View mRoot;
    private View mLoadingView;
    private ImageView mImageView;

    private Rect mRect;
    private String mPath;

    private ImageBus mImageBus;
    private boolean mFocused;
    private boolean mAnimated;

    private ImageDraggingLayout mImageDraggingLayout;

    public static Fragment newInstance(Rect rect, String url, boolean focused) {
        Fragment fragment = new PreviewFragment();
        Bundle data = new Bundle();
        data.putParcelable(EXTRA_RECT, rect);
        data.putString(EXTRA_URL, url);
        data.putBoolean(EXTRA_FOCUSED, focused);
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ImageBus) {
            mImageBus = (ImageBus) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRect = getArguments().getParcelable(EXTRA_RECT);
            mPath = getArguments().getString(EXTRA_URL);
            mFocused = getArguments().getBoolean(EXTRA_FOCUSED);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_preview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated: path = " + mPath);
        mRoot = view;
        setupView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupView() {
        mImageView = findViewById(R.id.img_photo);
        mLoadingView = findViewById(R.id.loading);

        mImageDraggingLayout = findViewById(R.id.layout_dragging);
        mImageDraggingLayout.initWith(mRect, mImageBus);
        fetchImage();
    }

    private void fetchImage() {
        Glide.with(this)
                .load(mPath)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onResourceReady(Drawable resource,
                                                   Object model,
                                                   Target<Drawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        mLoadingView.setVisibility(View.GONE);
                        mImageDraggingLayout.setBitmap(resource.getIntrinsicWidth(), resource.getIntrinsicHeight());
                        boolean animate = mFocused && !mAnimated;
                        mImageDraggingLayout.showAnimated(animate);
                        mAnimated = true;
                        return false;
                    }

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }
                })
                .transition(withCrossFade())
                .into(mImageView);
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T findViewById(@IdRes int id) {
        return (T) mRoot.findViewById(id);
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "onDestroyView: path = " + mPath);
        super.onDestroyView();
    }
}
