package com.hamzeh.easydroid.preview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class ImagesViewPagerAdapter extends FragmentPagerAdapter {
    public static final String TAG = ImagesViewPagerAdapter.class.getSimpleName();

    private ImagesHolder mImagesHolder;
    private int mIndex;

    public ImagesViewPagerAdapter(FragmentManager manager, ImagesHolder imagesHolder, int index) {
        super(manager);
        mImagesHolder = imagesHolder;
        mIndex = index;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "position: " + position + " url: " + mImagesHolder.imageUrls.get(position));
        return PreviewFragment.newInstance(mImagesHolder.imageRects.get(position),
                mImagesHolder.imageUrls.get(position), (mIndex == position));
    }

    @Override
    public int getCount() {
        return mImagesHolder.count();
    }
}