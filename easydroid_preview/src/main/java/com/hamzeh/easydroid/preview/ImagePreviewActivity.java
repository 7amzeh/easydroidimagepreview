package com.hamzeh.easydroid.preview;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.hamzeh.easydriod.preview.R;

public class ImagePreviewActivity extends AppCompatActivity implements ImageBus {
    public static String EXTRA_IMAGES_HOLDER = "IMAGES_HOLDER";
    public static String EXTRA_INDEX = "INDEX";
    public static String EXTRA_RECEIVER = "RECEIVER";

    public static int RESULT_CODE_SWITCH = 1;
    public static int RESULT_CODE_ANIMATION_END = 2;

    private ViewPager mViewPager;

    private ResultReceiver mResultReceiver;
    private ImagesHolder mImagesHolder;
    private int mIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mImagesHolder = getIntent().getParcelableExtra(EXTRA_IMAGES_HOLDER);
        mResultReceiver = getIntent().getParcelableExtra(EXTRA_RECEIVER);
        mIndex = getIntent().getIntExtra(EXTRA_INDEX, 0);

        mViewPager = findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(1);
        ImagesViewPagerAdapter adapter = new ImagesViewPagerAdapter(getSupportFragmentManager(),
                mImagesHolder, mIndex);
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(mIndex);
        mViewPager.removeOnPageChangeListener(mOnPageChangeListener);
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
        mOnPageChangeListener.onPageSelected(mIndex);
    }

    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int index) {
            Bundle data = new Bundle();
            data.putInt(EXTRA_INDEX, index);
            mResultReceiver.send(RESULT_CODE_SWITCH, data);
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }
    };

    @Override
    protected void onPause() {
        overridePendingTransition(0, 0);
        super.onPause();
    }

    @Override
    public void close() {
        mResultReceiver.send(RESULT_CODE_ANIMATION_END, new Bundle());
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        mResultReceiver.send(RESULT_CODE_ANIMATION_END, new Bundle());
        super.onDestroy();
    }
}