package com.hamzeh.easydroid.preview;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hamzeh.easydriod.preview.R;

public class ImageDraggingLayout extends RelativeLayout {
    private static final String TAG = ImageDraggingLayout.class.getSimpleName();
    private static final long DURATION = 350;

    private ImageView mPictureImageView;
    private FrameLayout mInnerWrapperFrameLayout;
    private ViewDragHelper mDragHelper;

    private Rect mOriginalRect;
    private ImageBus mImageBus;

    private FrameLayout.LayoutParams mImageViewLayoutParams;
    private RelativeLayout.LayoutParams mInnerWrapperLayoutParams;

    private int mDraggingState;
    private int mDraggingBorder;
    private int mInitialDraggingBorder;
    private int mDimAlpha;

    private int mFullHeight;
    private int mFullWidth;

    private int mFullImageWidth;
    private int mFullImageHeight;

    private int mFromLeft, mToLeft, mDeltaLeft,
            mFromTop, mToTop, mDeltaTop,
            mFromRight, mToRight, mDeltaRight,
            mFromBottom, mToBottom, mDeltaBottom;

    public ImageDraggingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        mPictureImageView = findViewById(R.id.img_photo);
        mImageViewLayoutParams = (FrameLayout.LayoutParams) mPictureImageView.getLayoutParams();

        mInnerWrapperFrameLayout = findViewById(R.id.layout_image_wrapper);
        mInnerWrapperLayoutParams = (RelativeLayout.LayoutParams) mInnerWrapperFrameLayout.getLayoutParams();

        mDragHelper = ViewDragHelper.create(this, 1.0f, new DragHelperCallback());
        super.onFinishInflate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.i(TAG, "onSizeChanged: w = " + w + " h = " + h);
        mFullWidth = w;
        mFullHeight = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void onStopDraggingToClosed() {
        Log.i(TAG, "onStopDraggingToClosed");
    }

    private void onStartDragging() {
        Log.i(TAG, "onStartDragging");
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        Log.i(TAG, "onInterceptTouchEvent");
        return isQueenTarget(event) && mDragHelper.shouldInterceptTouchEvent(event);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isQueenTarget(event) || isMoving()) {
            mDragHelper.processTouchEvent(event);
            return true;
        } else {
            return super.onTouchEvent(event);
        }
    }

    private boolean isQueenTarget(MotionEvent event) {
        int[] queenLocation = new int[2];
        mPictureImageView.getLocationOnScreen(queenLocation);
        int upperLimit = queenLocation[1] + mPictureImageView.getMeasuredHeight();
        int lowerLimit = queenLocation[1];
        int y = (int) event.getRawY();
        return (y > lowerLimit && y < upperLimit);
    }

    @Override
    public void computeScroll() { // needed for automatic settling.
        if (mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public boolean isMoving() {
        return (mDraggingState == ViewDragHelper.STATE_DRAGGING ||
                mDraggingState == ViewDragHelper.STATE_SETTLING);
    }

    public void setBitmap(int width, int height) {
        float heightRatio = height / (float) mFullHeight;
        float widthRatio = width / (float) mFullWidth;

        if (heightRatio > widthRatio) {
            mFullImageWidth = (int) (width / heightRatio);
            mFullImageHeight = (int) (height / heightRatio);
        } else {
            mFullImageWidth = (int) (width / widthRatio);
            mFullImageHeight = (int) (height / widthRatio);
        }
    }

    public void showAnimated(boolean animate) {
        if (mFullImageWidth == 0 || mFullImageHeight == 0) {
            mImageBus.close();
            return;
        }

        Log.i(TAG, "showAnimated, animate = " + animate);
        if (mFullImageHeight / (float) mOriginalRect.height() > mFullImageWidth / (float) mOriginalRect.width()) {
            mFromLeft = mOriginalRect.left;
            mToLeft = (mFullWidth - mFullImageWidth) / 2;

            mFromTop = mOriginalRect.top - getStatusBarHeight();
            mToTop = 0;

            mFromRight = mFullWidth - mOriginalRect.right;
            mToRight = (mFullWidth - mFullImageWidth) / 2;

            mFromBottom = mFullHeight - mOriginalRect.bottom + getStatusBarHeight();
            mToBottom = 0;
        } else {
            mFromLeft = mOriginalRect.left;
            mToLeft = 0;

            mFromTop = mOriginalRect.top - getStatusBarHeight();
            mToTop = (mFullHeight - mFullImageHeight) / 2;

            mFromRight = mFullWidth - mOriginalRect.right;
            mToRight = 0;

            mFromBottom = mFullHeight - mOriginalRect.bottom + getStatusBarHeight();
            mToBottom = mFullHeight - (mFullHeight + mFullImageHeight) / 2;
        }

        mDeltaLeft = mToLeft - mFromLeft;
        mDeltaTop = mToTop - mFromTop;
        mDeltaRight = mToRight - mFromRight;
        mDeltaBottom = mToBottom - mFromBottom;

        ValueAnimator expandAnimation = ValueAnimator.ofFloat(0.0f, 100.0f);
        expandAnimation.setDuration(animate ? DURATION : 0);
        expandAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                float animProgress = (Float) animator.getAnimatedValue();

                int leftMargin = (int) (mFromLeft + mDeltaLeft * animProgress / 100.0f);
                int topMargin = (int) (mFromTop + mDeltaTop * animProgress / 100.0f);
                int rightMargin = (int) (mFromRight + mDeltaRight * animProgress / 100.0f);
                int bottomMargin = (int) (mFromBottom + mDeltaBottom * animProgress / 100.0f);
                mInnerWrapperLayoutParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);

                if (mFullImageHeight / (float) mOriginalRect.height() > mFullImageWidth / (float) mOriginalRect.width()) {
                    mImageViewLayoutParams.width = mFullImageWidth;
                    mImageViewLayoutParams.height = mImageViewLayoutParams.width * mFullImageHeight / mFullImageWidth;
                } else {
                    mImageViewLayoutParams.height = mFullImageHeight;
                    mImageViewLayoutParams.width = mImageViewLayoutParams.height * mFullImageWidth / mFullImageHeight;
                }
                mInnerWrapperFrameLayout.requestLayout();

                mDimAlpha = Math.min(255, (int) (animProgress * 255 * 2 / 100));
                int col = Color.argb(mDimAlpha, 0, 0, 0);
                setBackgroundColor(col);
            }
        });
        expandAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mInitialDraggingBorder = mToTop;
                mInnerWrapperFrameLayout.setTop(mToTop);
                Log.i(TAG, "onAnimationEnd, mInitialDraggingBorder = " + mInitialDraggingBorder);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        expandAnimation.start();
    }

    /***
     * Callback
     */

    public class DragHelperCallback extends ViewDragHelper.Callback {
        @Override
        public void onViewDragStateChanged(int state) {
            if (state == mDraggingState) { // no change
                return;
            }
            if ((mDraggingState == ViewDragHelper.STATE_DRAGGING ||
                    mDraggingState == ViewDragHelper.STATE_SETTLING) && state == ViewDragHelper.STATE_IDLE) {
                if (mDraggingBorder == 0) {
                    onStopDraggingToClosed();
                }
            }
            if (state == ViewDragHelper.STATE_DRAGGING) {
                onStartDragging();
            }
            mDraggingState = state;
        }


        @Override
        public void onViewPositionChanged(@NonNull View changedView, int left, int top, int dx, int dy) {
            super.onViewPositionChanged(changedView, left, top, dx, dy);
            mDraggingBorder = top;

            int fullDimDrag = mFullHeight / 2;
            mDimAlpha = 255 * Math.abs(mDraggingBorder - mInitialDraggingBorder) / fullDimDrag;
            mDimAlpha = Math.max(0, 255 - Math.min(255, mDimAlpha));
            int col = Color.argb(mDimAlpha, 0, 0, 0);
            setBackgroundColor(col);
        }

        @Override
        public int getViewVerticalDragRange(@NonNull View child) {
            Log.i(TAG, "getViewVerticalDragRange");
            return mFullHeight;
        }

        @Override
        public boolean tryCaptureView(@NonNull View view, int i) {
            Log.i(TAG, "tryCaptureView");
            return (view.getId() == R.id.layout_image_wrapper);
        }

        @Override
        public int clampViewPositionVertical(@NonNull View child, int top, int dy) {
            Log.i(TAG, "clampViewPositionVertical: top = " + top + " dy = " + dy);
            return top;
        }

        @Override
        public int clampViewPositionHorizontal(@NonNull View child, int left, int dx) {
            return (mFullWidth - mFullImageWidth) / 2;
        }

        @Override
        public void onViewReleased(@NonNull View releasedChild, float xvel, float yvel) {
            Log.i(TAG, "onViewReleased");
            super.onViewReleased(releasedChild, xvel, yvel);

            if (mFullImageWidth == 0 || mFullImageHeight == 0) {
                mImageBus.close();
                return;
            }

            final int draggingY = mInnerWrapperFrameLayout.getTop() - mInitialDraggingBorder;
            boolean smallDrag = Math.abs(draggingY) < mFullHeight / 10;
            int settleLeft = (mFullWidth - mFullImageWidth) / 2;
            if (smallDrag) {
                Log.i(TAG, "smallDrag = true : " + draggingY);
                if (mDragHelper.settleCapturedViewAt(settleLeft, mInitialDraggingBorder)) {
                    Log.i(TAG, "settleCapturedViewAt = true");
                    ViewCompat.postInvalidateOnAnimation(ImageDraggingLayout.this);
                } else {
                    Log.i(TAG, "settleCapturedViewAt = false");
                    mInnerWrapperFrameLayout.setTop(mInitialDraggingBorder);
                }
                return;
            } else {
                Log.i(TAG, "smallDrag = false : " + draggingY +
                        " mInnerWrapperFrameLayout.getTop = " + mInnerWrapperFrameLayout.getTop() +
                        " mInitialDraggingBorder = " + mInitialDraggingBorder);
            }

            mDeltaTop += draggingY;
            mDeltaBottom -= draggingY;

            final int fromDim = mDimAlpha;
            final int deltaDim = -mDimAlpha;

            ValueAnimator collapseAnimation = ValueAnimator.ofFloat(0.0f, 100.0f);
            collapseAnimation.setDuration(DURATION);
            collapseAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    float animProgress = (Float) animator.getAnimatedValue();

                    int alpha = (int) (fromDim + deltaDim * animProgress / 100.0f);
                    int col = Color.argb(alpha, 0, 0, 0);
                    setBackgroundColor(col);

                    int leftMargin = (int) (mToLeft - mDeltaLeft * animProgress / 100.0f);
                    int topMargin = (int) (mToTop - mDeltaTop * animProgress / 100.0f) + draggingY;
                    int rightMargin = (int) (mToRight - mDeltaRight * animProgress / 100.0f);
                    int bottomMargin = (int) (mToBottom - mDeltaBottom * animProgress / 100.0f) - draggingY;
                    mInnerWrapperLayoutParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);

                    if (mFullImageHeight / (float) mOriginalRect.height() > mFullImageWidth / (float) mOriginalRect.width()) {
                        mImageViewLayoutParams.width = mInnerWrapperFrameLayout.getWidth();
                        mImageViewLayoutParams.height = mImageViewLayoutParams.width * mFullImageHeight / mFullImageWidth;
                    } else {
                        mImageViewLayoutParams.height = mInnerWrapperFrameLayout.getHeight();
                        mImageViewLayoutParams.width = mImageViewLayoutParams.height * mFullImageWidth / mFullImageHeight;
                    }
                    mInnerWrapperFrameLayout.requestLayout();
                }
            });
            collapseAnimation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mImageBus.close();
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            });
            collapseAnimation.start();
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void initWith(Rect originalRect, ImageBus imageBus) {
        mOriginalRect = originalRect;
        mImageBus = imageBus;
    }
}

