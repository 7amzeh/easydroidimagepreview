package com.hamzeh.easydroid.preview;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ImagesHolder implements Parcelable {
    public List<String> imageUrls = new ArrayList<>();
    public List<Rect> imageRects = new ArrayList<>();
    public List<View> views = new ArrayList<>();

    public ImagesHolder() {
    }

    protected ImagesHolder(Parcel in) {
        imageUrls = in.createStringArrayList();
        imageRects = in.createTypedArrayList(Rect.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(imageUrls);
        dest.writeTypedList(imageRects);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImagesHolder> CREATOR = new Creator<ImagesHolder>() {
        @Override
        public ImagesHolder createFromParcel(Parcel in) {
            return new ImagesHolder(in);
        }

        @Override
        public ImagesHolder[] newArray(int size) {
            return new ImagesHolder[size];
        }
    };

    public void append(String url, Rect rect, View view) {
        imageUrls.add(url);
        imageRects.add(rect);
        views.add(view);
    }

    public int count() {
        return Math.min(imageUrls.size(), imageRects.size());
    }
}
