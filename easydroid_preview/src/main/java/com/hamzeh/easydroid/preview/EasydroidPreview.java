package com.hamzeh.easydroid.preview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import java.util.List;

public class EasydroidPreview implements PreviewResultReceiver.Receiver {
    private PreviewResultReceiver mReceiver;
    private ImagesHolder mImagesHolder;

    public EasydroidPreview() {
        mReceiver = new PreviewResultReceiver(new Handler());
        mReceiver.setReceiver(this);
    }

    public void preview(Context context, ImagesHolder imagesHolder) {
        preview(context, imagesHolder, 0);
    }

    public void preview(Context context, ImagesHolder imagesHolder, int index) {
        mImagesHolder = imagesHolder;
        Intent intent = new Intent(context, ImagePreviewActivity.class);
        intent.putExtra(ImagePreviewActivity.EXTRA_IMAGES_HOLDER, imagesHolder);
        intent.putExtra(ImagePreviewActivity.EXTRA_INDEX, index);
        intent.putExtra(ImagePreviewActivity.EXTRA_RECEIVER, mReceiver);
        context.startActivity(intent);
    }

    public EasydroidPreview.Builder builder() {
        return new EasydroidPreview.Builder();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == ImagePreviewActivity.RESULT_CODE_SWITCH) {
            int index = resultData.getInt(ImagePreviewActivity.EXTRA_INDEX);
            List<View> views = mImagesHolder.views;
            for (int i = 0; i < views.size(); i++) {
                View view = views.get(i);
                if (view != null) {
                    view.setAlpha(i == index ? 0.1f : 1.0f);
                }
            }
            Log.i("INDEX", "index = " + index);
        } else if (resultCode == ImagePreviewActivity.RESULT_CODE_ANIMATION_END) {
            for (View view : mImagesHolder.views) {
                if (view != null) {
                    view.setAlpha(1.0f);
                }
            }
        }
    }

    public class Builder {
        private ImagesHolder mImagesHolder = new ImagesHolder();

        private Builder() {
        }

        public EasydroidPreview.Builder append(String url) {
            return append(url, null);
        }

        public EasydroidPreview.Builder append(String url, @Nullable View view) {
            mImagesHolder.append(url, calcRect(view), view);
            return this;
        }

        private Rect calcRect(@Nullable View view) {
            if (view == null) {
                return new Rect();
            }
            Rect rect = new Rect();
            view.getGlobalVisibleRect(rect);

            int location[] = new int[2];
            view.getLocationOnScreen(location);

            if (rect.height() != view.getHeight()) {
                if (rect.top == location[1]) {
                    rect.bottom = rect.top + view.getHeight();
                } else {
                    rect.top = rect.bottom - view.getHeight();
                }
            }
            // TODO: handle horizontal cropping
            return rect;
        }

        public void preview(Context context) {
            EasydroidPreview.this.preview(context, mImagesHolder);
        }

        public ImagesHolder getImageHolder() {
            return mImagesHolder;
        }
    }
}
