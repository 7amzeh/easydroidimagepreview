package com.hamzeh.easydriod.controller.interfaces;

import com.hamzeh.easydriod.model.server.Response;

public interface OnResponseReady {
    void onSuccess(Response response);

    void onFailure(Response response);
}