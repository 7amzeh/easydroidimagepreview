package com.hamzeh.easydriod.controller.interfaces;

import android.os.Bundle;
import android.view.View;

import com.hamzeh.easydriod.view.items.ListItem;

public interface ListItemCallback {
    void onItemClicked(View view, ListItem listItem, int position);

    void onItemAction(int action, View view, Bundle data);
}