package com.hamzeh.easydriod.controller.interfaces;

public interface Searchable {
    String getSearchKey();
}