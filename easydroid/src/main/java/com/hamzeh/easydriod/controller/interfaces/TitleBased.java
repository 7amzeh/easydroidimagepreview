package com.hamzeh.easydriod.controller.interfaces;

public interface TitleBased {
    String getTitle();
}