package com.hamzeh.easydriod.controller.interfaces;

public interface Releasable {
    void release();
}