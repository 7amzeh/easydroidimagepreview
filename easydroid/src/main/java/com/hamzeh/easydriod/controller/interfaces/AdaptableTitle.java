package com.hamzeh.easydriod.controller.interfaces;

public interface AdaptableTitle {
    void setActionbarTitle(int titleResId);

    void setActionbarTitle(String title);
}