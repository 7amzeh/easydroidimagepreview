package com.hamzeh.easydriod.controller.enums;

public enum ResponseMode {
    LIVE,
    CACHE
}