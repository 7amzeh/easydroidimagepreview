package com.hamzeh.easydriod.controller.interfaces;

/**
 * Created by Mahmoud  on 19/7/2018
 */
public interface OnDialogButtonClickListener {
    void onPositiveButtonClicked();
    void onNegativeButtonClicked();
}
