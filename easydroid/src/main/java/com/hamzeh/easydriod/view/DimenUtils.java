package com.hamzeh.easydriod.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;

public class DimenUtils {
    public static int pixelsToDp(Context context, int dimenId) {
        float dimen = context.getResources().getDimension(dimenId);
        return (int) (dimen / context.getResources().getDisplayMetrics().density);
    }

    public static Point getScreenSize(Context context) {
        int width = context.getResources().getDisplayMetrics().widthPixels;
        int height = context.getResources().getDisplayMetrics().heightPixels;
        return new Point(width, height);
    }

    public static int getStatusBarHeight(Resources res) {
        int result = 0;
        int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = res.getDimensionPixelSize(resourceId);
        }
        return result;
    }
}