package com.hamzeh.easydriod.view.items;

import com.hamzeh.easydriod.view.viewholders.SpaceViewHolder;

import org.hamzeh.easydroid.R;

public class SpaceItem extends ListItem {
    public static ListItemType SPACE_ITEM = new ListItemType(SpaceViewHolder.class, R.layout.item_space);

    private static final int CODE = 666;

    private int mHeight;

    public SpaceItem(int height) {
        mHeight = height;
    }

    @Override
    public ListItemType getListItemType() {
        return SPACE_ITEM;
    }

    @Override
    public int hashCode() {
        return CODE;
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof SpaceItem;
    }

    public int getHeight() {
        return mHeight;
    }
}