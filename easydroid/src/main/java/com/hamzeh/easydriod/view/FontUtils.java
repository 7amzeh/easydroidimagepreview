package com.hamzeh.easydriod.view;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontUtils {
    public enum CustomFont {
        OPEN_SANS_REGULAR("OpenSans-Regular.ttf"),
        OPEN_SANS_SEMI_BOLD("OpenSans-Semibold.ttf"),
        ALEF_REGULAR("Alef-Regular.ttf"),
        LATO_REGULAR("Lato-Regular.ttf"),
        LATO_BOLD("Lato-Bold.ttf");

        private String mFontName;

        CustomFont(String fontName) {
            mFontName = fontName;
        }

        public String getFontName() {
            return mFontName;
        }
    }

    private static Map<CustomFont, Typeface> customFonts;

    public static Typeface getFont(Context ctx, CustomFont customFont) {
        if (customFonts == null) {
            customFonts = new HashMap<>();
        }

        if (!customFonts.containsKey(customFont) || customFonts.get(customFont) == null) {
            Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + customFont.getFontName());
            customFonts.put(customFont, typeface);
        }

        return customFonts.get(customFont);
    }
}