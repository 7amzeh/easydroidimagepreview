package com.hamzeh.easydriod.view.easydialog.dialogs;

import android.content.Context;
import android.view.View;

import com.hamzeh.easydriod.view.custom.CustomTextView;

import org.hamzeh.easydroid.R;

public class CustomDialog extends BaseDialog implements View.OnClickListener{
    private CustomTextView titleTextView;
    private CustomTextView msgTextView;
    private CustomTextView positiveButton;
    private CustomTextView negativeButton;

    public CustomDialog(Context context, View.OnClickListener onClickListener) {
        super(context, onClickListener);
    }

    @Override
    protected void setupView() {
        super.setupView();
        titleTextView = dialogView.findViewById(R.id.txt_title);
        msgTextView = dialogView.findViewById(R.id.txt_msg);
        preparePositiveButton();
        prepareNegativeButton();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_custom_material;
    }

    private void prepareNegativeButton() {
        negativeButton = dialogView.findViewById(R.id.btn_negative);
        negativeButton.setOnClickListener(this);
        negativeButton.setTag(mTag);
    }

    private void preparePositiveButton() {
        positiveButton = dialogView.findViewById(R.id.btn_positive);
        positiveButton.setOnClickListener(this);
        positiveButton.setTag(mTag);
    }

    public void draw(int titleResId,
                     int msgResId,
                     int positiveButtonCaptionResId,
                     int negativeButtonCaptionResId) {
        String msg = getContext().getString(msgResId);
        draw(titleResId, msg, positiveButtonCaptionResId, negativeButtonCaptionResId);
    }

    public void draw(int titleResId,
                     String msg,
                     int positiveButtonCaptionResId,
                     int negativeButtonCaptionResId) {
        titleTextView.setText(titleResId);
        msgTextView.setText(msg);
        positiveButton.setText(positiveButtonCaptionResId);
        if (negativeButtonCaptionResId == 0) {
            negativeButton.setVisibility(View.GONE);
        } else {
            negativeButton.setVisibility(View.VISIBLE);
            negativeButton.setText(negativeButtonCaptionResId);
        }
    }

    @Override
    public void onClick(View v) {
        if (mOnClickListener != null && animationStatus == BaseDialog.STATUS_EXPANDED) {
            mOnClickListener.onClick(v);
        }
    }
}