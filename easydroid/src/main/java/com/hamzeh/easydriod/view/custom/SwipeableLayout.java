package com.hamzeh.easydriod.view.custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.hamzeh.easydriod.view.DimenUtils;

import org.hamzeh.easydroid.R;

public class SwipeableLayout extends FrameLayout {
    public static String TAG = SwipeableLayout.class.getSimpleName();

    private static final int MIN_DURATION = 250;
    private static final float SPEED_TO_HIDE = 20.0f;
    private static final float SCREEN_RATIO_TO_HIDE = 0.3f;

    private static final int THRESHOLD_UP = -5;
    private static final int THRESHOLD_DOWN = 5;
    private static final int ANIMATION_STEP = 1;

    private RecyclerView mRecyclerView;

    private RelativeLayout.LayoutParams mLpUp;
    private RelativeLayout.LayoutParams mLpDown;

    private View mSpaceUp;
    private View mSpaceDown;

    private Handler mAnimationHandler;

    private boolean mAnimating;

    public SwipeableLayout(Context context) {
        super(context);
        init(null);
    }

    public SwipeableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SwipeableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mAnimationHandler = new Handler();

        inflate(getContext(), R.layout.custom_swipeable_layout, this);
        FrameLayout frameLayout = findViewById(R.id.layout_swipeable_container);

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SwipeableLayout);
            int layoutResId = typedArray.getResourceId(R.styleable.SwipeableLayout_sw_layout, 0);
            LayoutInflater.from(getContext()).inflate(layoutResId, frameLayout, true);

            int recyclerViewResId = typedArray.getResourceId(R.styleable.SwipeableLayout_sw_recycler_view, 0);
            mRecyclerView = findViewById(recyclerViewResId);
            typedArray.recycle();
        }

        mSpaceUp = findViewById(R.id.space_upper);
        mSpaceDown = findViewById(R.id.space_lower);
        mLpUp = (RelativeLayout.LayoutParams) mSpaceUp.getLayoutParams();
        mLpDown = (RelativeLayout.LayoutParams) mSpaceDown.getLayoutParams();
    }

    private float mFirstY;
    private float mLastY;
    private float mDeltaY;
    private float mDraggedFromY;
    private float mTargetHeight;

    private long mTimePressedDown;

    private boolean mDragging;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                Log.i(TAG, "dispatchTouchEvent: ACTION_DOWN");
                mTimePressedDown = System.currentTimeMillis();
                mFirstY = mLastY = event.getY();
                mDragging = false;
                break;
            }
            case MotionEvent.ACTION_UP: {
                Log.i(TAG, "dispatchTouchEvent: ACTION_UP");
                mDragging = false;
                if (mLpUp.height > 0 || mLpDown.height > 0) {
                    hidePage();
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (event.getY() != mLastY) {
                    mDeltaY = event.getY() - mLastY;
                    mLastY = event.getY();
                }
                if (!mDragging && (mRecyclerView.getAdapter() == null || !mRecyclerView.canScrollVertically(THRESHOLD_UP))) {
                    mDragging = true;
                    mDraggedFromY = event.getY();

                    mAnimationHandler.removeCallbacksAndMessages(null);
                    mAnimationHandler.postDelayed(mAnimationRunnable, ANIMATION_STEP);
                }
                if (mDragging) {
                    mTargetHeight = Math.max(0, event.getY() - mDraggedFromY);
                    Log.i(TAG, "DeltaY = " + ((int) mTargetHeight - mLpUp.height));
                    Log.i(TAG, "TargetHeight = " + mTargetHeight);
                }
                break;
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void hidePage() {
        mAnimating = true;
        final int screenHeight = DimenUtils.getScreenSize(getContext()).y;

        int from = 0, to = 0;

        float speed = Math.abs(mLastY - mFirstY) / (System.currentTimeMillis() - mTimePressedDown)
                * 100 / getResources().getDisplayMetrics().density;
        Log.i(TAG, "Speed: " + speed);

        if (mDeltaY > 0) {
            if (mLpUp.height > screenHeight * SCREEN_RATIO_TO_HIDE || speed > SPEED_TO_HIDE) {
                from = mLpUp.height;
                to = screenHeight;
            } else {
                from = mLpUp.height;
                to = 0;
            }
        } else if (mDeltaY < 0) {
            from = mLpUp.height;
            to = 0;
        }

        int displacement = Math.abs(from - to);
        int duration = Math.max(MIN_DURATION, displacement / 5);

        ValueAnimator animator = ValueAnimator.ofInt(from, to);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mLpUp.height = (Integer) valueAnimator.getAnimatedValue();
                mSpaceUp.requestLayout();
                mRecyclerView.scrollToPosition(0);
                if (mLpUp.height == screenHeight) {
                    closeActivity();
                }
            }
        });

        animator.setDuration(duration);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimating = false;
            }
        });
        animator.start();
    }

    private void closeActivity() {
        if (getContext() instanceof Activity) {
            Activity activity = (Activity) getContext();
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    private Runnable mAnimationRunnable = new Runnable() {
        @Override
        public void run() {
            mAnimationHandler.removeCallbacksAndMessages(null);
            if (mDragging) {
                int delta = (int) Math.abs(mTargetHeight - mLpUp.height);
                int step = delta / 3;

                if (mTargetHeight > mLpUp.height) {
                    mLpUp.height += step;
                } else {
                    mLpUp.height -= step;
                }

                mSpaceUp.requestLayout();
                mRecyclerView.scrollToPosition(0);

                mAnimationHandler.postDelayed(this, ANIMATION_STEP);
            }
        }
    };
}