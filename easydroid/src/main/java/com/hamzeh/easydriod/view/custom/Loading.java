package com.hamzeh.easydriod.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.pnikosis.materialishprogress.ProgressWheel;

import org.hamzeh.easydroid.R;

public class Loading extends RelativeLayout {
    private ProgressWheel progressWheel;

    public Loading(Context context) {
        super(context);
        init();
    }

    public Loading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Loading(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.custom_loading, this);
        progressWheel = findViewById(R.id.progress_wheel);
    }

    public void spin() {
        progressWheel.spin();
    }

    public void stopSpinning() {
        progressWheel.stopSpinning();
    }
}