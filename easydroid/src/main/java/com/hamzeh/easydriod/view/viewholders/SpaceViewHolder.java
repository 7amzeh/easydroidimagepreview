package com.hamzeh.easydriod.view.viewholders;

import android.view.View;

import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.SpaceItem;

import org.hamzeh.easydroid.R;

public class SpaceViewHolder extends EasydroidViewHolder {
    private View mSpaceView;

    public SpaceViewHolder(View itemView) {
        super(itemView);
        mSpaceView = findViewById(R.id.space);
    }

    @Override
    public void draw(ListItem listItem) {
        super.draw(listItem);
        SpaceItem spaceItem = (SpaceItem) listItem;
        mSpaceView.getLayoutParams().height = spaceItem.getHeight();
        mSpaceView.requestLayout();
    }
}