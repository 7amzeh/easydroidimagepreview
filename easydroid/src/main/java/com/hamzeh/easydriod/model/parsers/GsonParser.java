package com.hamzeh.easydriod.model.parsers;

import android.util.Log;

import com.google.gson.Gson;

public class GsonParser extends BaseParser {
    public static final String TAG = GsonParser.class.getSimpleName();

    @Override
    public Object parse(Class responseClass, String json) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(json, responseClass);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, json == null ? "JSON is null" : json);
        }
        return null;
    }
}