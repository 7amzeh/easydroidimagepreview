package com.hamzeh.easydriod.model.parsers;

public class NoParser extends BaseParser {
    @Override
    public Object parse(Class responseClass, String json) {
        return json;
    }
}