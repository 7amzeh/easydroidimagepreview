package com.hamzeh.easydriod.model.server;

import android.net.Uri;
import android.util.Log;

import com.hamzeh.easydriod.model.parsers.BaseParser;
import com.hamzeh.easydriod.model.parsers.GsonParser;

import java.util.LinkedHashMap;
import java.util.Map;

public class WebService {
    public static String JWT_PARAM_NAME = "needle";
    public static String JWT_KEY = "1234";
    public static int sType;

    private String mUrl;
    private String mApi;

    private int mType;

    private boolean mWithJwt;

    private Class mParserClass;
    private Class mResponseClass;
    private EasydroidHttp.WebserviceType mWebserviceType;
    private String[] mParams;

    public WebService() {
        mParserClass = GsonParser.class;
        mWebserviceType = EasydroidHttp.WebserviceType.GET;
        mParams = new String[]{};
        mType = sType++;
    }

    @Override
    public int hashCode() {
        return mType;
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof WebService && mType == ((WebService) other).mType;
    }

    private String paramsToString(Map<String, String> params) {
        if (params == null || params.isEmpty()) {
            return "";
        }
        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
        return builder.build().getEncodedQuery();
    }

    public Map<String, String> getParamsMap(String[] values) {
        Map<String, String> paramsMap = new LinkedHashMap<>();
        int count = Math.min(mParams.length, values.length);
        for (int i = 0; i < count; i++) {
            if (values[i] != null) {
                paramsMap.put(mParams[i], values[i]);
            }
        }
        return paramsMap;
    }

    public Map<String, String> getJwtParamsMap(String[] values) {
        Map<String, String> paramsMap = new LinkedHashMap<>();
        int count = Math.min(mParams.length, values.length);
        if (count == 0) return paramsMap;
        JParamsBuilder builder = JParamsBuilder.newInstance();
        for (int i = 0; i < count; i++) {
            if (values[i] != null) {
                builder.append(mParams[i], values[i]);
            }
        }
        paramsMap.put(JWT_PARAM_NAME, builder.build());
        Log.i("needle", paramsMap.get(JWT_PARAM_NAME));
        return paramsMap;
    }

    /*** Setters & Getters */

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getUrlWithApi() {
        return mUrl + mApi;
    }

    public String getFullUrl(String... values) {
        return getUrlWithApi() + "?" + paramsToString(getParamsMap(values));
    }

    public boolean withJwt() {
        return mWithJwt;
    }

    public String getApi() {
        return mApi;
    }

    public void setApi(String api) {
        mApi = api;
    }

    public String[] getParams() {
        return mParams;
    }

    public Class getParserClass() {
        return mParserClass;
    }

    public Class getResponseClass() {
        return mResponseClass;
    }

    public EasydroidHttp.WebserviceType getWebServiceType() {
        return mWebserviceType;
    }

    /*** Params Builder */

    public static Builder builder() {
        return new WebService.Builder();
    }

    public static class Builder {
        private WebService instance = new WebService();

        private Builder() {
        }

        public Builder get(String url) {
            instance.mUrl = url;
            instance.mWebserviceType = EasydroidHttp.WebserviceType.GET;
            return this;
        }

        public Builder post(String url) {
            instance.mUrl = url;
            instance.mWebserviceType = EasydroidHttp.WebserviceType.POST;
            return this;
        }

        public Builder postMultipart(String url) {
            instance.mUrl = url;
            instance.mWebserviceType = EasydroidHttp.WebserviceType.POST_MULTIPART;
            return this;
        }

        public Builder put(String url) {
            instance.mUrl = url;
            instance.mWebserviceType = EasydroidHttp.WebserviceType.PUT;
            return this;
        }

        public Builder delete(String url) {
            instance.mUrl = url;
            instance.mWebserviceType = EasydroidHttp.WebserviceType.DELETE;
            return this;
        }

        public Builder api(String api) {
            instance.mApi = api;
            return this;
        }

        public Builder parserClass(Class<? extends BaseParser> parserClass) {
            instance.mParserClass = parserClass;
            return this;
        }

        public Builder responseClass(Class responseClass) {
            instance.mResponseClass = responseClass;
            return this;
        }

        public Builder withJwt() {
            instance.mWithJwt = true;
            return this;
        }

        public Builder params(String... params) {
            instance.mParams = params;
            return this;
        }

        public WebService build() {
            return instance;
        }
    }
}

