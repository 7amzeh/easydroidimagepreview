package com.hamzeh.easydriod.model.server;

public abstract class JsonBody {
    public abstract String toJson();
}