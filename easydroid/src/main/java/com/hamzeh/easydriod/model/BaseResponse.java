package com.hamzeh.easydriod.model;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("success")
    public boolean success;
    @SerializedName("response_code")
    public int responseCode;
    @SerializedName("error_code")
    public int errorCode;
    @SerializedName("message")
    public String message;
    @SerializedName("exec_time")
    public float execTime;

    public BaseResponse() {
    }
}
